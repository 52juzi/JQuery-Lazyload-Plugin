###  **JQuery Lazyload Plugin** 
JQuery Lazyload Plugin 基于JQuery的延迟加载插件。设置浏览器底部偏移量，当监听元素抵达即可触发延迟加载函数。

内置样式使用bootstrap3

[在线演示](http://harris992.oschina.io/jquery-lazyload-plugin)

### Download 下载
> src/jquery-lazyload.js

### Import script 标签引入
```
<script src="js/jquery-lazyload.js"></script>
```

### initialized 初始化
```
$('.panel').lazyload({
    bottom: 100,
    lazyHandler: function (element) {
        // TODO: loadHandler...
    }
})
```
### Core options 选项
- bottom	integer	        20	底部偏移量 offsetButtom (元素探出底部偏移量)
- lazyHandler	function	{}	延迟加载处理函数